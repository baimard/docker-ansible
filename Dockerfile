FROM python:latest

RUN pip install pip --upgrade
RUN pip install ansible
RUN pip install ansible-core
RUN pip install six

RUN apt-get update -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    sshpass

WORKDIR /work